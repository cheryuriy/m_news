from .serializers import *
from .permissions import AdminOnly
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import permissions
from rest_framework import mixins
from rest_framework import filters
from django_filters import FilterSet, ChoiceFilter, MultipleChoiceFilter, DateFilter, ModelMultipleChoiceFilter, OrderingFilter
from django.db.models import Count


@api_view(['GET'])
def api_root(request, format=None):
    """DRF API in root url.

        3 types of API will be rendered:
        1) For not loged in.
        2) For users-guests.
        3) For super_users additional:
             -guest-list
             -drafts-list
    """
    user = request.user
    if not user.is_authenticated:
        return Response({
            'registration': reverse('registration', request=request, format=format),})
    urls = {
        'news': reverse('news-list', request=request, format=format),
        'authors':reverse('author-list', request=request, format=format),
        'categorys':reverse('category-list', request=request, format=format),
        'tags': reverse('tag-list', request=request, format=format),
        }
    if not user.is_superuser:
        return Response(urls)
    urls['users'] = reverse('guest-list', request=request, format=format)
    urls['drafts'] = reverse('drafts-list', request=request, format=format)
    return Response(urls)


class Registration(generics.CreateAPIView):
    """Anyone can register."""

    queryset = Guest.objects.all()
    serializer_class = GuestSerializer
    permission_classes = set()


class GuestList(generics.ListAPIView):
    """List of registered guests for admins."""

    queryset = Guest.objects.all()
    serializer_class = GuestSerializer
    permission_classes = (AdminOnly,)


class NewsDraftsList(generics.ListAPIView):
    """List of news Drafts for Admin.

        Can be filtered by filter_fields.
    """

    queryset = News.objects.filter(published=False)
    serializer_class = NewsSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('id', 'created', 'category', 'published', 'tags', 'content', 'author')
    permission_classes = (AdminOnly,)


class AuthorList(generics.ListAPIView):
    """List of authors of news.

        Can be filtered by all fields.
    """

    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('__all__')
    permission_classes = (permissions.IsAuthenticated,)


class CategoryList(generics.ListAPIView):
    """List of news categories.

        Can be filtered by all fields.
    """

    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('__all__')
    permission_classes = (permissions.IsAuthenticated,)


class TagList(generics.ListAPIView):
    """List of news tags.

        Can be filtered by all fields.
    """

    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('tag', 'id')
    permission_classes = (permissions.IsAuthenticated,)


def get_choices():
    """Tuple of news tags for searching in MyFilterSet."""

    all_tags = tuple((str(t.id), str(t.tag)) for t in Tag.objects.all())
    return all_tags

CHOICES = (('0', 'Ascending'),('1', 'Descending'))

class MyFilterSet(FilterSet):
    """Custom FilterSet.

        News can be filtered by date: "after_created", "before_created",
        or ordered by number of photos: "num_photos",
        or filtered by news tags:
            1)"tags" - All tags must be present
            2)"tags__id" - At least one tag should be present.
    """

    after_created = DateFilter(label="After date:", name="created", lookup_expr='gt')
    before_created = DateFilter(label="Before date:", name="created", lookup_expr='lt')
    tags = MultipleChoiceFilter(label="All tags must be present", choices=get_choices(), method = 'choose')
    tags__id = ModelMultipleChoiceFilter(label="At least one tag:", queryset=Tag.objects.all(),
                                         name='tags__id', to_field_name='id')
    num_photos = ChoiceFilter(label="Sort by number of photos", choices=CHOICES, method='orders_by_photo_count')


    def orders_by_photo_count(self, queryset, name, value):
        if '1' in value:
            return queryset.order_by('-num_photos')
        elif '0' in value:
            return queryset.order_by('num_photos')
        return queryset


    def choose(self, qs, name, value): # For tags
        for val in value:
            qs = qs.filter(tags__id__in=val)
        return qs

    class Meta:
        model = News
        fields = ('before_created', 'after_created', 'tags', 'tags__id', 'num_photos')


class CustomDjangoFilterBackend(DjangoFilterBackend):
    """Without it, not custom filters would work with customs."""

    default_filter_set = MyFilterSet


class NewsList(generics.ListAPIView):
    """List of all News.

        Can be filtered, ordered and searched by fields below
        and MyFilterSet.
    """

    queryset = News.objects.filter(published=True).annotate(num_photos=Count('photo'))
    serializer_class = NewsSerializer  # NewsPartSerializer
    filter_backends = (CustomDjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('id', 'created', 'category', 'content', 'author')
    search_fields = ('content', 'tags__tag', 'category__name',  'author__name')
    ordering_fields = ('created', 'category', 'author')
    permission_classes = (permissions.IsAuthenticated,)


class CategoryDetail(mixins.RetrieveModelMixin,
                  generics.GenericAPIView):
    """Retrieving of 1 category with all it's news."""

    queryset = Category.objects.all()
    serializer_class = FullCategorySerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class NewsDetail(mixins.RetrieveModelMixin,
                      generics.GenericAPIView):
    """Retrieving of 1 news."""

    queryset = News.objects.filter(published=True)
    serializer_class = NewsSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
