from rest_framework import permissions
from django.http import Http404


class AdminOnly(permissions.BasePermission):
    """If user are not admin, Http404 will be raised."""

    def has_permission(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return True
        raise Http404