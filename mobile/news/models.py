from django.db import models
from django.contrib.auth.models import User


class Guest(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    avatar = models.ImageField(upload_to='avatars', null=True)


class Author(models.Model):
    name = models.CharField(max_length=40, null=False)
    born = models.DateField(blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, default='')

    class Meta:
        ordering = ('name',)

class Category(models.Model):
    name = models.CharField(max_length=50, null=False, unique=True)
    higher_category = models.ForeignKey('self', null=True, blank=True, related_name='subcategory', on_delete=models.CASCADE)

    class Meta:
        ordering = ('name',)

class Tag(models.Model):
    tag = models.CharField(max_length=50, null=False)

    class Meta:
        ordering = ('tag',)

class News(models.Model):
    created = models.DateField(auto_now_add=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    published = models.BooleanField(default=False)      #  Черновик = False
    tags = models.ManyToManyField(Tag)
    content = models.TextField()
    main_photo = models.ImageField(upload_to='main_photos', null=True)


class Commentary(models.Model):
    content = models.CharField(max_length=400, null=False)
    guest = models.ForeignKey(Guest, on_delete=models.CASCADE)
    news = models.ForeignKey(News, on_delete=models.CASCADE)


class Photo(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='photos', null=True) # Better to CREATE CATALOG


    def __str__(self):
        return self.photo.name