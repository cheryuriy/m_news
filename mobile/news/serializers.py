from rest_framework import serializers
from django.contrib.auth.models import User
from .models import *
from rest_framework_recursive.fields import RecursiveField


NGINX_URL = "http://127.0.0.1:80/media/"  # - NGINX are Providing Photos


class UserPartSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name')


class GuestViewSerializer(serializers.HyperlinkedModelSerializer):
    """These 2 serializers for user fields that guests can see."""

    user = UserPartSerializer()

    class Meta:
        model = Guest
        fields = ('avatar', 'user')


class UserSerializer(serializers.ModelSerializer):
    password= serializers.CharField( style={'input_type': 'password'} )
    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'last_name', 'first_name', 'email')


class GuestSerializer(serializers.HyperlinkedModelSerializer):
    """These 2 serializers for all user fields, only admin can see."""

    user = UserSerializer()

    class Meta:
        model = Guest
        fields = ('avatar', 'user')

    def create(self, validated_data):
        """Creation of guest-user."""

        user_data = validated_data.pop('user')
        user = User.objects.create_user(**user_data)
        user.is_staff = False
        user.is_superuser = False
        user.save()
        Guest.objects.create(user=user, **validated_data)
        return Guest


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = ('name', 'born', 'description')


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    """All categories are recursively connected with higher categories.
        id - link to see 1 category.
    """

    id = serializers.HyperlinkedRelatedField(view_name='category-detail', read_only=True)
    higher_category = RecursiveField(allow_null=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'higher_category')


class TagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'tag')



class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    """Photos are represented by photo_urls, which are rendered by server like Nginx."""

    photo_url = serializers.SerializerMethodField()

    def get_photo_url(self, photo):
        return NGINX_URL + str(photo)

    class Meta:
        model = Photo
        fields = ('photo_url',)


class CommentarySerializer(serializers.HyperlinkedModelSerializer):
    """Commentary include guest - creator."""

    guest = GuestViewSerializer()

    class Meta:
        model = Commentary
        fields = ('id', 'guest', 'content')


class NewsSerializer(serializers.HyperlinkedModelSerializer):
    """Full news serializer for all fields."""

    category = CategorySerializer()
    author = AuthorSerializer()
    commentary_set = CommentarySerializer(read_only=True, many=True)
    tags = TagSerializer(read_only=True, many=True)
    photo_set = PhotoSerializer(read_only=True, many=True)
    id = serializers.HyperlinkedRelatedField(view_name='news-detail', read_only=True)
    photo_url = serializers.SerializerMethodField()

    def get_photo_url(self, news):
        return NGINX_URL + str(news.main_photo.name)

    class Meta:
        model = News
        fields = ('id', 'photo_url', 'tags', 'category', 'content', 'photo_set', 'created', 'author', 'published', 'commentary_set')


class NewsPartSerializer(serializers.HyperlinkedModelSerializer):
    """Short form of news, without photos and comments."""

    category = CategorySerializer()
    tags = TagSerializer(read_only=True, many=True)
    id = serializers.HyperlinkedRelatedField(view_name='news-detail', read_only=True)
    photo_url = serializers.SerializerMethodField() # MAIN photo
    # photo_set = PhotoSerializer(read_only=True, many=True)

    def get_photo_url(self, news):
        return NGINX_URL + str(news.main_photo.name)

    class Meta:
        model = News
        fields = ('id', 'photo_url', 'tags', 'category') # , 'photo_set')


class FullCategorySerializer(serializers.HyperlinkedModelSerializer):
    """If yoy are watching 1 category, you could see all of it's news in short form."""

    id = serializers.HyperlinkedRelatedField(view_name='category-detail', read_only=True)
    news = serializers.SerializerMethodField()

    def get_news(self, category):
        news = category.news_set.filter(published = True)
        serializer = NewsPartSerializer(news, context=self.context, many=True)
        return serializer.data

    class Meta:
        model = Category
        fields = ('id', 'name', 'higher_category', 'news')


